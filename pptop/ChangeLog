2013-02-10  Samuel Thibault  <samuel.thibault@ens-lyon.org>

	Fix build against libpthread.

	* aux.c: Include pthread.h instead of cthreads.h.
	* configure.ac: Link against libpthread instead of libthreads. Also
	link libihash, libshouldbeinlibc and libm.
	* options.c: Include pthread.h instead of cthreads.h. Use pthread
	functions and types instead of cthreads functions and types.
	* pptop.c: Likewise.

2006-06-21  Alfredo Beaumont Sainz  <alfredo.beaumont@gmail.com>

	* configure.ac: Updated contact email to
	<hurdextras-hacker@nongnu.org>.

	* pptop.c: Include <hurd/ihash.h>.
	(new_update): Changed type of ihash_t to hurd_ihash_t.  Use
	hurd_ihash_create instead of ihash_create.

2002-08-26  James A. Morrison  <ja2morri@uwaterloo.ca>

	* configure.ac: Bumped the version to 0.1.1.
	* Makefile.am (pptop_SOURCES): Added header files to pptop_SOURCES,
	now a release tarball will be able to compile.
	* NEWS: Added news item for the 0.1.1 release.
	* filter.h: Removed ajust_filter declaration.
	Moved filter #define's to ...
	* filter.c: ... Here.
	(show_filters): New Function.  Shows and edits filter 
	states.
	* help.c (top_help): Document new options, f, d, and z.
	* options.c: Include <math.h> for rounding functions.
	(wait_and_check_for_quit): Added new options, f, d, and z.
	(set_delay): New Function.  Sets the time between updates.

2002-08-25  James A. Morrison  <ja2morri@uwaterloo.ca>

	* pptop.c: Updated to use interfaces in filter.h, title.h, options.h,
	and fields.h.
	* Makefile.am (pptop_SOURCES): Added filter.c, title.c, options.c, and
	fields.c.
	* title.c: New file.
	(set_title): New function.
	(get_title): New function.
	(get_title_pos): New function.
	* title.h: New file.  Public declarations for title.c
	* filter.c: New file. 
	New filters: user_filter, not_leader_filter, and unorphaned_filter.
	(struct active_filter): Struct to hold filters and their states.
	(adjust_filter): New Function.  
	(do_filters): New Function.
	(change_user): New Function.
	(top_user_filter): New private function.
	(setup_filters): New Function.  Must be called before do_filters can
	be used.
	* filter.h: New file.  Declares public functions and #define's 
	in filter.c.
	* options.h: New file.  Declares public functions in options.c.
	* options.c: New file.
	(wait_and_check_for_quit): ... Moved to here.  Added new
	options (c, u, e).
	(get_lines): New function.  Returns the number of lines to print.
	(set_lines): New function.  Sets the number of lines to print.
	(get_string): New function.  Reads a string from the user.
	(get_num): New function.  Reads a integral number from the user.
	(struct fork_type): New structure.  Used to pass arguments to get_num
	and get_string.
	* configure.ac: Moved ALL_LINGUAS to before AM_GNU_GETTEXT.  Catalogs
	now get built.
	* help.c (top_help): Documented new commands (c, u, e).
	* aux.c (curses_print_strings): Call clrtoeol () after printing
	a string to ensure there is no overwrite.
	(wait_and_check_for_quit): Moved to options.c.
	(get_display_proc_num): Function Removed.  It wasn't general enough.

2002-08-23  James A. Morrison  <ja2morri@uwaterloo.ca>

	* pptop.c: Include "config.h".
	(main): Prep for localization, call setlocale, bindtextdomain, and
	textdomain.
	* autogen.sh: New file.  Generates all files needed to build pptop.
	* Makefile: Removed.
	* Makefile.am: New file.  To be processed by automake.

2002-08-21  James A. Morrison  <ja2morri@uwaterloo.ca>

	* Makefile: Use LDLIBS.  Link pptop against libthreads.
	* pptop.c (new_update): Limit lines printed to LINES_TO.
	(new_update): Print IO message if HEADER_LOCK is used.
	* aux.c (wait_and_check_for_quit): Added the '#' command to get
	the number of processes.
	(wait_and_check_for_quit): Don't try to getch if a thread is waiting 
	for IO.
	(curses_print_strings): limit the number of lines printed to LIMIT.
	(get_display_proc_num): New function.  Gets the number of processes to
	display.
	* help.c (top_help): Use LINE to determine which line to display
	a help line on.  Document '#' command.

2002-08-20  James A. Morrison  <ja2morri@uwaterloo.ca>

	* pptop.c (new_update): Lookup _SERVERS_DEFPAGER if we can't get
	the host port.

2002-08-19  James A. Morrison  <ja2morri@uwaterloo.ca>

	* specs.h (ps_specs): Named Command to Commandline.  New format Command
	for specifying only the program name.
	* aux.c: Include <specs.h>.
	(wait_and_check_for_quit): Added options to control the display of the
	header lines (l, m, s).  Added Sorting options (N, n, P, p, T, t).
	* help.c (top_help): Documented new options.
	* pptop.c: Created global variables: *headers, *global_fmt_string,
	*sort_key_name, and sort_key_reverse.
	Moved *_LINE and TABLE_HEADER defintions to...
	* aux.h: ... Here.
	* pptop.c (setup_headers): New function to initialize header lines.
	(fix_head_count): New function to organize the header lines.
	(adjust_headers): New function to turn on/off header lines.

2002-06-16  James A. Morrison  <ja2morri@uwaterloo.ca>

	* help.c: New file.
	(top_help): New function to print out available commands.
	* help.h: New File.  Header file for help.c
	* aux.c (wait_and_check_for_quit): Added switch for available commands.
	* Makefile (CC): Removed excess targets and flags.

2002-05-26  James A. Morrison  <ja2morri@uwaterloo.ca>

	* pptop.c (new_update): Use proc_stat_state instead of STATE variable.
	(new_update): Add unknown process state.

2002-05-25  James A. Morrison  <ja2morri@uwaterloo.ca>

	* pptop.c (new_update): Added memory line.
	* aux.c (curses_print_strings): Added parameter OFFSET.

2002-05-24  James A. Morrison  <ja2morri@uwaterloo.ca>

	* aux.c (init_ps_stream): Remove extra variables and comments.
	(do_curses_setup): Likewise.
	(get_strum_string): Likewise. Try getting clear memory.
	(count_strings): Function removed.
	* pptop.c (new_update): Added %state to LOCAL_FMT_STRING.
	(new_update): New embedded function find_proc_state.

2002-05-21  James A. Morrison  <ja2morri@uwaterloo.ca>

	* pptop.c (new_update): Show pager information, if available.

2002-05-20  James A. Morrison  <ja2morri@uwaterloo.ca>

	* aux.c: Ran through GNU ident.
	* pptop.c: Ran through GNU ident.
	(wait_and_check_for_quit): Expect ERR to be returned from getch () if
	no key is pressed.

2002-04-13  Paul Emsley  <paule@chem.gla.ac.uk>

	* Release 0.0.4
	
2002-03-26 Jon Arney <jarney1@cox.net>
	* Fixed couple of core dump problems in 'strum_string' handling
	  code.
	* Added a little 'uptime' printout just for giggles.
	
2001-10-17  James Morrison <ja2morri@uwaterloo.ca>
        * Attempted memory cleanups

2001-10-17  Paul Emsley  <paule@chem.gla.ac.uk>

	* ChangeLog started [should have done it earlier - heyho]. 
	

