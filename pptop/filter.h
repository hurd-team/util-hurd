/* filter declaraions for pptop

   Copyright (C) 2002 James A. Morrison.

   Written by James A. Morrison <ja2morri@uwaterloo.ca>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111, USA */


/* This function runs all active filters across PSL */
int do_filters (struct proc_stat_list *psl);

/* This function must be called when top begins */
int setup_filters ();

/* Changes the user has his/her processes being shown.  If NAME is NULL or
   invalid then all users will have their processes shown */
int change_user (char *name);

/* Shows the filters that are available and lets the user turn on and off
   the filterst that they want.  Returns the number of state changes that
   occured. */
int show_filters ();
