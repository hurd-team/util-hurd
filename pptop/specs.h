/* Augment the standard specs with our own abbreviations for pptop.

   Copyright (C) 2001, 2002 Paul Emsley.

   Written by Paul Emsley <paule@chem.gla.ac.uk>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111, USA */

static const struct ps_fmt_spec
spec_abbrevs[] = {
  {"TT=tty"}, {"SC=susp"}, {"Stat=state"}, {"Commandline=args"}, {"SL=sleep"},
  {"NTH=nth", "TH"}, {"NI=bpri"}, {"SZ=vsize"}, {"RSS=rsize"},
  {"MsgI=msgin"}, {"MsgO=msgout"}, {"Command=Arg0"},
  {0}
};
static struct ps_fmt_specs ps_specs =
{ spec_abbrevs, &ps_std_fmt_specs };

