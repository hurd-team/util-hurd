/* help functions for pptop

   Copyright (C) 2002 James A. Morrison.

   Written by James A. Morrison <ja2morri@uwaterloo.ca>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111, USA */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif 

#include <unistd.h>
#include <ncurses.h>
#include <libintl.h>

#include "aux.h"

#define _(STRING) gettext (STRING)

void
top_help ()
{
  int line = 1;

  erase ();

  mvaddstr (line++, 0, _("q\tQuit"));
  mvaddstr (line++, 0, _("l\tToggle Load Average"));
  mvaddstr (line++, 0, _("#\tSet the number of processes to show"));
  mvaddstr (line++, 0, _("m\tToggle display of memory information"));
  mvaddstr (line++, 0, _("s\tToggle display of summary information"));
  mvaddstr (line++, 0, _("e\tToggle thread listing")); 
  mvaddstr (line++, 0, _("c or C\tToggle display of command line arguments"));

  mvaddstr (line++, 0, _("P or p\tSort by CPU usage"));
  mvaddstr (line++, 0, _("N or n\tSort by PID"));
  mvaddstr (line++, 0, _("T or t\tSort by time"));

  mvaddstr (line++, 0, _("u\tShow only a specified user"));

  mvaddstr (line++, 0, _("d\tSet the delay between screen updates.  Max precision 0.1 seconds"));
  mvaddstr (line++, 0, _("z\tPause Screen"));
  mvaddstr (line++, 0, _("h or ?\tPrint this help list"));
  mvaddstr (line++, 0, _("f\tEdit filters"));

  bold_text (LINES - 1, 0, _("Press any key to continue"));
  while (getch () == ERR) usleep (10000);
}
