/* Auxillary functions for pptop

   Copyright (C) 2001, 2002 Paul Emsley.
   Copyright (C) 2002 James A. Morrison.

   Written by Paul Emsley <paule@chem.gla.ac.uk>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111, USA */

#include <stdio.h>
#include <string.h>
#include <ncurses.h>

#include <ps.h>

void do_curses_setup ();
void undo_curses();
void bold_text(int line, int col, char *str);
void rv_text(int line, char *str);

int wait_and_check_for_quit();
int init_ps_stream(struct ps_stream *strum);


void control(void);
char **string_splitter(char*, int nlines);
char *next_breaker(char *);
void print_strings(char **str_list); 
void curses_print_strings(char **str_list, int limit, int offset); 

int proc_stat_list_fmt_analogue(struct ps_stream *strum);
char*         get_strum_string(struct ps_stream *strum);

void count_strings(char **str_list, int );

void log_char(char *); 

char *extend_string(char * *in_str, int new_length, char filler);

void print_lines_cols(void); 

int max(int, int);

#define UPTIME_LINE 0
#define PROCESS_LINE 1
#define CPU_LINE     2
#define MEMORY_LINE  3
#define PAGER_LINE   4
#define LOADMACH_LINE 5
#define TABLE_HEADER 6
