typedef enum
{
  MBOXERR_FILE
} mbox_err_t;

typedef (* f)(nodename, dir, mtime) nodecreator_t;

mbox_err_t
mbox_parse (char* file, HEADER **mbox, int (* notify_hdr)(HEADER* hdr));
