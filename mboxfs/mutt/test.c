/* This is just a small compilation/debugging test. */
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "mutt.h"
#include "mailbox.h"

CONTEXT main_context;

int
new_hdr (HEADER *hdr)
{
  /* do nothing */
  return 0;
}

int
main ()
{
  bzero (&main_context, sizeof (CONTEXT));
  main_context.path = strdup ("mbox"); /* XXX */
  main_context.notify_new_header = new_hdr;
  main_context.magic = M_MBOX;
  main_context.readonly = 1;
  assert (mbox_open_mailbox (&main_context) == 0);
  return 0;
}
