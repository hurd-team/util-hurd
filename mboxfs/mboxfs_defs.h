/* mboxfs - Creates a filesystem based on the contents of a mailbox.
   Copyright (C) 2002, Ludovic Court�s <ludo@chbouib.org>
 
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or * (at your option) any later version.
 
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

/*
 * Mboxfs common definitions.
 */

#ifndef __MBOXFS_DEFS_H__
#define __MBOXFS_DEFS_H__

#include <hurd/netfs.h>
#include "mutt/mutt.h"

/******************************************************************************
 * General user definitions
 ******************************************************************************/

/* The main directories. */
#define DIR_SENDERS "from"
#define DIR_RCPT    "to"
#define DIR_DATE    "date"
#define DIR_THREADS "threads"
#define DIR_ATTACH  "attach"

/* Node name used when the subject field of an email is empty. */
#define NAME_NOSUBJECT "<nosubject>"
/* Node name prefix for nameless attachments. */
#define NAME_ATTACH    "attach"

/* Attachments cache size. */
#define ATTACH_CACHE_SIZE 4096


/******************************************************************************
 * Internal mboxfs definitions
 ******************************************************************************/

/* struct mboxfs_opts */
struct mboxfs_opts
{
  char* file_name;	/* mailbox file name. */
  int   force_mmdf;	/* when true, parse the mailbox as an mmdf one. */
  int   show_headers;	/* tells whether email headers should appear. */
  int   threaded;	/* tells whether mailbox should be parsed in
			   another thread to avoid startup timeout. */
};

/* struct attach_info */
struct attach_info
{
  BODY   *body;

  struct
  {
    /* cache information */
    size_t start;	/* start offset of the data cached */
    size_t end;		/* end offset of the data cached   */
    size_t offs;	/* actual offset in mbox file (may be different from start!) */
    char   buffer[ATTACH_CACHE_SIZE];
  } cache;
};

typedef enum
{
  N_NODE = 0,		/* a regular node */
  N_SENDERS_BASE,	/* the DIR_SENDERS directory. */
  N_SENDER,		/* a sender's directory. */
  N_RCPT_BASE,		/* the DIR_RCPT directory. */
  N_RCPT,		/* a recipeint's directory. */
  N_ATTACH_BASE,	/* the DIR_ATTACH directory. */
  N_ATTACH,		/* an email's attachments dir */
  N_THREADS_BASE,	/* the DIR_THREADS directory. */
  N_THREAD,		/* a thread's directory */
  N_DATES_BASE,		/* the DIR_DATE directory */
  N_DATE		/* a date directory */
}
mboxfs_node_t;

/* struct mboxfs_info */
struct mboxfs_info
{
  mboxfs_node_t type;	/* type of the node */
  HEADER *header;	/* corresponding email header (NULL for attachments) */
  struct node *sender;	/* corresponding sender's node (in DIR_SENDERS)      */
  struct node *attach_dir; /* the 1st DIR_ATTACH created for this N_NODE node */
  struct attach_info *attach; /* for an attachment: corresponding body (else NULL) */
};

/* The following macros take struct node *_N as an argument. */
#define NEW_NODE_INFO(_n) \
  (_n)->nn->info = calloc (1, sizeof (struct mboxfs_info));
#define NODE_INFO(_n) \
  ((struct mboxfs_info*)((_n)->nn->info))
#define NEW_ATTACH_INFO(_a) \
  NODE_INFO(_a)->attach = \
    (struct attach_info*) calloc (1, sizeof(struct attach_info));
#define CACHE_INFO(_a)  (NODE_INFO(_a)->attach->cache)

#endif
