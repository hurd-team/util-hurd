/* mboxfs - Creates a filesystem based on the contents of a mailbox.
   Copyright (C) 2002, Ludovic Court�s <ludo@chbouib.org>
 
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or * (at your option) any later version.
 
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

/* The Mailbox File System backend.
 * (mboxfs)
 */

#ifndef __MBOXFS_H__
#define __MBOXFS_H__

#include "backend.h"
#include "mboxfs_defs.h"

/* Our backend definition */
extern struct fs_backend  mboxfs_backend;

/* filesystem options */
extern struct mboxfs_opts mboxfs_options;

#endif

