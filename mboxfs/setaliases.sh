#!/bin/sh

alias t='cd ~/trans/sortfs'
alias s='settrans -fgca ./tnode ./mboxfs mbox'
alias p='ps aux | grep mboxfs'
alias g='gdb mboxfs'
