/* mboxfs - Creates a filesystem based on the contents of a mailbox.
   Copyright (C) 2002, Ludovic Court�s <ludo@type-z.org>
 
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or * (at your option) any later version.
 
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

/*
 * General filesystem node management facilities.
 */

#ifndef __FSUTILS_H__
#define __FSUTILS_H__

#include "xmlfs.h"

#include <hurd.h>
#include <hurd/netfs.h>
#include <fcntl.h>

#define SUBST_SLASH '|'

/* Initialization.
 */
extern int fs_init ();

/* Creates a new node in directory DIR, with name NAME and mode M. If not NULL,
 * N points to the newly created node.
 * Checks whether there already exists such a node.
 * NAME is *not* duplicated!
 */
extern error_t fs_make_node (struct node **n, struct node *dir,
    char* name, mode_t m);

/* Used to add a sub-directory to DIR. If SUBDIRNAME already exists in DIR,
 * returns the number of entries in it; otherwise creates it and returns
 * zero. NEWDIR points to DIR/SUBDIRNAME.
 * It also checks whether SUBDIRNAME already exists.
 * SUBDIRNAME is *not* duplicated!
 */
extern unsigned long fs_make_subdir (struct node **newdir,
    struct node *dir, char *subdirname);

/* Returns the path of a given node (relatively to the given root node).
 */
extern char* get_path_from_root (struct node *root, struct node *node);

/* Returns the relavive path to the given root node.
 */
extern char* get_path_to_root (struct node *root, struct node *node);

/* Gets the first common directory.
 */
extern struct node* get_common_root (struct node *node1, struct node *node2);

/* Filters a node name, that is, remove '/' and chars lower than 32.
 * Returns NAME is no change has been made, or a pointer to a newly
 * malloced buffer otherwise.
 */
extern char* filter_node_name (char* name);

#endif /* __FSUTILS_H__ */
