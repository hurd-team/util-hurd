/* xmlfs -- a translator for accessing XML documents

   Copyright (C) 2002, 2005 HurdFR.
   Written by  Marc de Saint Sauveur <marc@hurdfr.org>
           and Manuel Menal          <mmenal@hurdfr.org>
   
   xmlfs is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   xmlfs is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA. 
*/

#ifndef __XML_H__
#define __XML_H__

#define _GNU_SOURCE 1

#include <hurd/netfs.h>

/* Find the node with pathname PATH. */
xmlNodePtr xml_find_node (const char *path);
/* Dump NODE to BUF and write the content size to SIZE. */
error_t xml_dump_node (xmlNodePtr node, char **buf, size_t *size);

/* Create all the children node in DIR. */
error_t fill_dirnode (struct node *dir);

#endif
