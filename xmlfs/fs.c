/* xmlfs -- a translator for accessing XML documents

   Copyright (C) 2002, 2005 HurdFR.
   Written by  Marc de Saint Sauveur <marc@hurdfr.org>
           and Manuel Menal          <mmenal@hurdfr.org>
   
   xmlfs is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   xmlfs is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA. 
*/

#include "xmlfs.h"
#include "fsutils.h"

FILE *debug;

error_t
xmlfs_create (file_t fd, struct xmlfs *xmlfs)
{
  error_t err;

  DEBUG ("NOTICE: %s\n", 
	 __PRETTY_FUNCTION__);

  err = fs_init ();
  assert (err == 0);

  /* Create root node. */
  netfs_root_node = NULL;
  err = fs_make_node (&netfs_root_node, NULL, NULL, S_IFDIR | 0755);
  
  if (err)
    return err;

  netfs_root_node->nn_stat.st_nlink = 2;

  xmlfs->root = netfs_root_node;

  DEBUG ("INFO: building XML tree...\n");

  /* Build an XML tree from the file */
  xmlfs->doc = xmlReadFd (fd, NULL, NULL, XML_PARSE_NOCDATA);
  
  if (xmlfs->doc == NULL)
    error (-1, 0, "E: (%s, %d): couldn't parse", 
	   __FILE__, __LINE__);
     
  xmlfs->root_node = xmlDocGetRootElement (xmlfs->doc);
  
  if (xmlfs->root_node == NULL) 
    {
      xmlFreeDoc (xmlfs->doc);
      error (-1, 0, "E: (%s, %d): couldn't get root element", 
	     __FILE__, __LINE__);
    }

  /* Make our pathname XPath compliant. */
  asprintf(&netfs_root_node->nn->pathname, "/%s", xmlfs->root_node->name);

  DEBUG ("NOTICE: %s returns with 0.\n", __PRETTY_FUNCTION__);

  return 0;
}
