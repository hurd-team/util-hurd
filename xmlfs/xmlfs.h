/* xmlfs -- a translator for accessing XML documents

   Copyright (C) 2002, 2005 HurdFR.
   Written by  Marc de Saint Sauveur <marc@hurdfr.org>
           and Manuel Menal          <mmenal@hurdfr.org>
   
   xmlfs is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   xmlfs is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA. 
*/

#ifndef __XMLFS_H__
#define __XMLFS_H__

#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <error.h>

#include <hurd/netfs.h>

#include <libxml/tree.h>

/* User data contained in each 'struct node' */
struct netnode 
{
  char *name; /* node name */
  char *pathname; /* path+node name. Should be valid XPath */

  bool filled;

  enum
    {
      CONTENT = '0',
      NODE = '1',
    } type;
   
  struct node *entries; /* directory entries, if this is a directory */
  struct node *dir; /* parent directory */
};

struct xmlfs
{
  struct node *root;

  xmlDocPtr doc;
  xmlNodePtr root_node;
};
    
extern struct xmlfs *xmlfs;  

error_t xmlfs_create (file_t, struct xmlfs *);

extern FILE *debug;
#define DEBUG(format, ...) if (debug) fprintf (debug, format, ## __VA_ARGS__)

#endif /* __XMLFS_H__ */
