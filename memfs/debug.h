#ifndef __DEBUG_H__
#define __DEBUG_H__

#define DEBUG_TRACE 5

extern void debuginit (int argc, char* argv[]);
extern void debugend ();
extern void debugsetlevel (int l);
extern void debuglog (int dlevel, const char *msg, ...);

#endif
