/* test-memfs-1.c -- simple write test to memfs-1 translated file.
   Copyright (C) 2000 Farid Hajji <farid.hajji@ob.kamp.net>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

/* $Id: memfs-test.c,v 1.1 2006/04/12 21:43:03 mmenal Exp $ */

/*
 * $Log: memfs-test.c,v $
 * Revision 1.1  2006/04/12 21:43:03  mmenal
 * Initial commit of a memfs written by Farid Hajji (Manuel)
 *
 * Revision 1.1  2000/12/19 18:55:40  farid
 * Initial revision
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main (int argc, char *argv[])
{
  int fdes;
  off_t offset, target_offset;
  int result;
  char buf[40];
  int i;

  if (argc < 3)
    {
      fprintf (stderr, "usage: %s memfile offset\n", argv[0]);
      exit (255);
    }
  target_offset = strtoul (argv[2], NULL, 10);

  fdes = open (argv[1], O_RDWR);
  if (fdes == -1)
    {
      perror ("open()");
      exit (1);
    }

  offset = lseek (fdes, target_offset, SEEK_SET);
  if (offset != -1)
      printf ("I am now at offset %ld\n", offset);
  else
    {
      perror ("lseek()");
      exit (2);
    }

  strncpy (buf, "0123456789", sizeof(buf));
  result = write (fdes, buf, strlen(buf));
  printf ("written %d bytes\n", result);

  offset = lseek (fdes, 0, SEEK_SET);
  if (offset != -1)
    printf ("I am now at offset %ld\n", offset);
  else
    {
      perror ("lseek() 2nd invocation");
      exit (3);
    }
  
  result = read (fdes, buf, sizeof(buf));
  if (result == -1)
    {
      perror ("read()");
      exit (4);
    }

  printf ("got %d bytes: [", result);
  for (i=0; i<result; ++i)
    printf ("%c", isprint(buf[i]) ? buf[i] : '.');
  printf ("]\n");

  result = close (fdes);
  if (result == -1)
    {
      perror ("close()");
      exit (5);
    }

  return 0;
}
