package Hurd;

use Exporter ();
@ISA = ('Exporter');
$VERSION = '0.003';

1;

=head1 NAME

Hurd - Perl support for writing Hurd servers

=head1 SYNOPSIS

  use Hurd;

=head1 DESCRIPTION

This module currently just provides $Hurd::VERSION, which is the
version of the Perl module, not the version of the Hurd.

=head1 COPYING

Copyright (C) 1999,2000  John Tobey, jtobey@john-edwin-tobey.org

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=head1 SEE ALSO

L<Hurd::Trans::Hello>.

=cut
