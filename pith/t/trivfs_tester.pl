#!/bin/perl -w

# Ensure that the trivfs callbacks get called appropriately.

use Hurd::Trivfs;
@ISA = ('Hurd::Trivfs');

use strict;

open STDOUT, ">t/trivfs.out"
    or die "Can't write trivfs.out: $!";

$| = 1;
my $data = 'start';
my $open_count = 0;

__PACKAGE__->start ('timeout' => 0);
exit;


sub goaway {
    print "goaway\n";
    close STDOUT;
    exit 0;
}

sub init_peropen {
    my ($self, $po) = @_;
    print "init_peropen: ".(++$open_count).", ".$po->openmodes."\n";
    $po->hook ($open_count);
}

sub io_write {
    my ($self, $prot, $buffer, $offset, $amount) = @_;
    print "io_write: ".$prot->po->hook.", $offset\n";
    $data = $buffer;
    return 119;
}

sub io_read {
    my ($self, $prot, $bufref, $offset, $amount) = @_;
    print "io_read: ".$prot->po->hook.", $offset\n";
    $$bufref = $data;
}

sub io_seek {
    my ($self, $prot, $offset, $whence) = @_;
    print "io_seek: ".$prot->po->hook.", $offset, $whence\n";
    return 253;
}

sub io_set_all_openmodes {
    my ($self, $prot, $modes) = @_;
    print "io_set_all_openmodes: ".$prot->po->hook.", $modes\n";
}

sub io_get_openmodes     {
    my ($self, $prot) = @_;
    print "io_get_openmodes: ".$prot->po->hook."\n";
    return 0777;
}

sub io_set_some_openmodes   {
    my ($self, $prot, $modes) = @_;
    print "io_set_some_openmodes: ".$prot->po->hook.", $modes\n";
}

sub io_clear_some_openmodes {
    my ($self, $prot, $modes) = @_;
    print "io_clear_some_openmodes: ".$prot->po->hook.", $modes\n";
}

sub file_set_size {
    my ($self, $prot, $size) = @_;
    print "file_set_size: ".$prot->po->hook.", $size\n";
    $data = '';
}
