# -*- cperl -*-

use Test;

BEGIN { $^W = 1; $| = 1; plan tests => 6; }
use Hurd::Trivfs;

END { unlink 't/_test_', 't/trivfs.out'; }

if (!-d 't') {
    die "must be run from the toplevel pith dist dir";
}

ok(1);  # loaded ok.

use Config;

$cmd = "settrans -ac t/_test_ $Config{perlpath}";
foreach (@INC) { $cmd .= " -I$_"; }
$cmd .= " t/trivfs_tester.pl";
#print "# $cmd\n";

!-e 't/_test_' or unlink 't/_test_' or die "Can't remove _test_: $!";
system ($cmd) and die "settrans failed";

use Fcntl;

sysopen (TEST1, "t/_test_", O_RDWR)        or die "sysopen: $!";
ok (syswrite (TEST1, "hello"), 119);
sysopen  (TEST2, "t/_test_", O_RDONLY)     or die "sysopen: $!";
syswrite (TEST1, "goodbye", 7, 0);
close TEST1;
ok (sysread  (TEST2, $buf, 7), 7);
ok($buf, 'goodbye');
close TEST2;
system "settrans -fg t/_test_";

@lines = `cat t/trivfs.out`;
ok (pop(@lines), "goaway\n");
ok (shift(@lines), "init_peropen: 1, ".O_RDWR."\n");

# XXX Other lines have some strangeness.
