/* Perl bindings for the Hurd trivfs library.
   Copyright (C) 1999,2000 John Tobey, jtobey@john-edwin-tobey.org

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */


#define filter_t perl_filter_t  /* header file conflict */

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#undef filter_t  /* header file conflict */

#include <hurd/trivfs.h>
#include <hurd/fd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

/* Error code to return when callbacks misbehave.  */
#define EPERL EDIED

struct hook
{
  void *orig;  /* libtrivfs object (protid, peropen, or control).  */
  SV *hook;    /* User data.  */
};

/* Return a new Hurd::Trivfs::Protid, Hurd::Trivfs::Peropen, or
   Hurd::Trivfs reference.  ORIG points to a struct trivfs_protid,
   trivfs_peropen, or trivfs_control.
   The referenced scalar points to a malloc'ed struct hook.
   The caller must store the returned value in ORIG->HOOK.  */
static SV *
alloc_hook (void *orig, HV *stash)
{
  struct hook *h;

  /* Perl's malloc ought to croak if out of memory.  */
  h = (struct hook *) malloc (sizeof *h);
  h->orig = orig;
  h->hook = &PL_sv_undef;
  return sv_bless (newRV_noinc (newSViv ((IV) h)), stash);
}

void
free_hook (void *hook)
{
  struct hook *h;

  h = (struct hook *) SvIV (SvRV ((SV *) hook));
  SvREFCNT_dec (h->hook);
  h->hook = &PL_sv_undef;
  /* XXX Should unbless *h.  */
  sv_setsv (hook, &PL_sv_undef);
  SvREFCNT_dec (h);
  free (h);
}

int trivfs_fstype = FSTYPE_MISC;  /* how about FSTYPE_PERL */
int trivfs_fsid = 0;
int trivfs_allow_open = O_READ | O_WRITE;
int trivfs_support_read = 1;
int trivfs_support_write = 1;
int trivfs_support_exec = 0;

static int
STORE (SV *param, int value)
{
  if (SvROK (param))
    switch (SvIV (SvRV (param)))
      {
      case 0: return trivfs_fstype = value;
      case 1: return trivfs_fsid = value;
      case 2: return trivfs_allow_open = value;
      case 3: return trivfs_support_read = value;
      case 4: return trivfs_support_write = value;
      case 5: return trivfs_support_exec = value;
      }
  return 0;
}

static int
FETCH (SV *param)
{
  if (SvROK (param))
    switch (SvIV (SvRV (param)))
      {
      case 0: return trivfs_fstype;
      case 1: return trivfs_fsid;
      case 2: return trivfs_allow_open;
      case 3: return trivfs_support_read;
      case 4: return trivfs_support_write;
      case 5: return trivfs_support_exec;
      }
  return 0;
}

static error_t peropen_init (struct trivfs_peropen *);
static void peropen_destroy (struct trivfs_peropen *);
static error_t protid_init (struct trivfs_protid *);
static void protid_destroy (struct trivfs_protid *);

error_t (*trivfs_peropen_create_hook) (struct trivfs_peropen *)
     = peropen_init;
void (*trivfs_peropen_destroy_hook) (struct trivfs_peropen *)
     = peropen_destroy;
error_t (*trivfs_protid_create_hook) (struct trivfs_protid *) = protid_init;
void (*trivfs_protid_destroy_hook) (struct trivfs_protid *) = protid_destroy;

struct cached_gv
{
  GV *gv;
  const char *name;
};

/* Single point of entry to the Perl interpreter.
   PUSH may modify sp; PULL must not.  That's why PULL is not called POP.  */
/* XXX This function would be useful to other modules... */
static error_t
call_sub (struct cached_gv *cb,
	  void (*push) (), error_t (*pull) ())
{
  dSP;
  error_t ret;
  SV *retsv;

  if (cb->gv == 0)
    {
      cb->gv = gv_fetchpv ((char *) cb->name, 1, SVt_PVCV);
      SvREFCNT_inc ((SV*) cb->gv);
    }

  ENTER;
  SAVETMPS;
  PUSHMARK (sp);

  if (push)
    (*push) ();

  perl_call_sv ((SV *) cb->gv, G_SCALAR | G_EVAL);
  SPAGAIN;
  retsv = POPs;
  PUTBACK;

  if (SvTRUE (ERRSV))
    {
      /* Print more useful information?  */
      warn ("%_ pid %_: %_",
	    perl_get_sv ("0", 1),
	    perl_get_sv ("$", 1),
	    ERRSV);
      ret = EPERL;
    }
  else
    {
      ret = (error_t) SvIV (retsv);
      if (ret == 0 && pull != 0)
	ret = (*pull) ();
    }

  FREETMPS;
  LEAVE;

  return ret;
}

/* Make a Perl array of the stat fields, pass it by reference to a method,
   and put the array elements back in the struct.  */
/* XXX The C interface should accept an error_t return value, because
   we might run out of memory or otherwise flop.  */
void
trivfs_modify_stat (struct trivfs_protid *cred, struct stat *st)
{
  static struct cached_gv sub = { 0, "Hurd::Trivfs::_::modify_stat" };
  AV *statav;

  void push ()
    {
      dSP;

      av_extend (statav, 16);

      /* Store the standard Perl stat fields in their usual order.  */
      av_store (statav, 0, newSViv ((I32) st->st_dev));
      av_store (statav, 1, newSViv ((I32) st->st_ino));
      av_store (statav, 2, newSViv ((I32) st->st_mode));
      av_store (statav, 3, newSViv ((I32) st->st_nlink));
      av_store (statav, 4, newSViv ((I32) st->st_uid));
      av_store (statav, 5, newSViv ((I32) st->st_gid));
      av_store (statav, 6, newSViv ((I32) st->st_rdev));
      av_store (statav, 7, newSViv ((I32) st->st_size));

      /* Perl stat returns integer times, but there's no harm in making
	 them floating-point under Hurd, right?  */
      av_store (statav, 8,
		     newSVnv (st->st_atim.tv_sec + 1e-9 * st->st_atim.tv_nsec));
      av_store (statav, 9,
		     newSVnv (st->st_mtim.tv_sec + 1e-9 * st->st_mtim.tv_nsec));
      av_store (statav, 10,
		     newSVnv (st->st_ctim.tv_sec + 1e-9 * st->st_ctim.tv_nsec));

      av_store (statav, 11, newSViv ((I32) st->st_blksize));
      av_store (statav, 12, newSViv ((I32) st->st_blocks));

      /* Finished with the standard stat, now tack on the extra GNU fields.  */
      av_store (statav, 13, newSViv ((I32) st->st_fstype));
      av_store (statav, 14, newSViv ((I32) st->st_gen));
      av_store (statav, 15, newSViv ((I32) st->st_author));
      av_store (statav, 16, newSViv ((I32) st->st_flags));


      XPUSHs ((SV *) cred->hook);
      XPUSHs (sv_2mortal (newRV ((SV *) statav)));

      PUTBACK;
    }

  error_t pull ()
    {
      double t;
      SV **tmp;

      tmp = av_fetch (statav, 0, 0);
      if (tmp) st->st_dev      = (dev_t)   SvIV (*tmp);
      tmp = av_fetch (statav, 1, 0);
      if (tmp) st->st_ino      = (ino_t)   SvIV (*tmp);
      tmp = av_fetch (statav, 2, 0);
      if (tmp) st->st_mode     = (mode_t)  SvIV (*tmp);
      tmp = av_fetch (statav, 3, 0);
      if (tmp) st->st_nlink    = (nlink_t) SvIV (*tmp);
      tmp = av_fetch (statav, 4, 0);
      if (tmp) st->st_uid      = (uid_t)   SvIV (*tmp);
      tmp = av_fetch (statav, 5, 0);
      if (tmp) st->st_gid      = (gid_t)   SvIV (*tmp);
      tmp = av_fetch (statav, 6, 0);
      if (tmp) st->st_rdev     = (dev_t)   SvIV (*tmp);
      tmp = av_fetch (statav, 7, 0);
      if (tmp) st->st_size     = (off_t)   SvIV (*tmp);

      tmp = av_fetch (statav, 8, 0);
      if (tmp)
	{
	  t = SvNV (*tmp);
	  /* anyone want to use modf() ? */
	  st->st_atim.tv_sec = (time_t) t;
	  st->st_atim.tv_nsec = (unsigned long int) 1e9 * (t - st->st_atime);
	}
      tmp = av_fetch (statav, 9, 0);
      if (tmp)
	{
	  t = SvNV (*tmp);
	  st->st_mtim.tv_sec = (time_t) t;
	  st->st_mtim.tv_nsec = (unsigned long int) 1e9 * (t - st->st_mtime);
	}
      tmp = av_fetch (statav, 10, 0);
      if (tmp)
	{
	  t = SvNV (*tmp);
	  st->st_ctim.tv_sec = (time_t) t;
	  st->st_ctim.tv_nsec = (unsigned long int) 1e9 * (t - st->st_ctime);
	}

      tmp = av_fetch (statav, 11, 0);
      if (tmp) st->st_blksize  = (unsigned int) SvIV (*tmp);
      tmp = av_fetch (statav, 12, 0);
      if (tmp) st->st_blocks   = (blkcnt_t)     SvIV (*tmp);

      tmp = av_fetch (statav, 13, 0);
      if (tmp) st->st_fstype   = (int)          SvIV (*tmp);
      tmp = av_fetch (statav, 14, 0);
      if (tmp) st->st_gen      = (unsigned int) SvIV (*tmp);
      tmp = av_fetch (statav, 15, 0);
      if (tmp) st->st_author   = (uid_t)        SvIV (*tmp);
      tmp = av_fetch (statav, 16, 0);
      if (tmp) st->st_flags    = (unsigned int) SvIV (*tmp);

      return 0;
    }

  statav = newAV ();
  (void) call_sub (&sub, &push, &pull);
  SvREFCNT_dec ((SV *) statav);
}

error_t
trivfs_goaway (struct trivfs_control *cntl, int flags)
{
  static struct cached_gv sub = { 0, "Hurd::Trivfs::_::goaway" };

  void push ()
    {
      dSP;
      XPUSHs ((SV *) cntl->hook);
      XPUSHs (sv_2mortal (newSViv ((IV) flags)));
      PUTBACK;
    }

  return call_sub (&sub, &push, 0);
}

/* XXX Should probably abscond with the peropen hooks and override
   trivfs_open.  */
static error_t
peropen_init (struct trivfs_peropen *peropen)
{
  error_t ret;
  static struct cached_gv sub = { 0, "Hurd::Trivfs::_::peropen_init" };

  void push ()
    {
      dSP;
      XPUSHs ((SV *) peropen->hook);
      PUTBACK;
    }

  /* XXX Should call a method of the Control which returns our object.  */
  peropen->hook = alloc_hook (peropen,
			      gv_stashpv ("Hurd::Trivfs::Peropen", 1));
  if (peropen->hook == 0)
    return ENOMEM;

  ret = call_sub (&sub, &push, 0);
  if (ret)
    free_hook (peropen->hook);

  return ret;
}

static void
peropen_destroy (struct trivfs_peropen *peropen)
{
  /* XXX Should call some Perl.  */
  free_hook (peropen->hook);
}

static error_t
protid_init (struct trivfs_protid *prot)
{
  error_t ret;
  static struct cached_gv sub = { 0, "Hurd::Trivfs::_::protid_init" };

  void push ()
    {
      dSP;
      XPUSHs ((SV *) prot->hook);
      PUTBACK;
    }

  /* XXX Should call a method of the Peropen which returns our object.  */
  prot->hook = alloc_hook (prot, gv_stashpv ("Hurd::Trivfs::Protid", 1));
  if (prot->hook == 0)
    return ENOMEM;

  ret = call_sub (&sub, &push, 0);
  if (ret)
    free_hook (prot->hook);

  return ret;
}

static void
protid_destroy (struct trivfs_protid *prot)
{
  /* XXX Should call some Perl.  */
  free_hook (prot->hook);
}


/* I/O interface */

kern_return_t
trivfs_S_io_write (struct trivfs_protid *cred,
		   mach_port_t reply, mach_msg_type_name_t replytype,
		   data_t data, mach_msg_type_number_t data_len,
		   off_t offs, mach_msg_type_number_t *amount)
{
  error_t ret;
  static struct cached_gv sub = { 0, "Hurd::Trivfs::_::io_write" };
  SV *amountsv;

  void push ()
    {
      dSP;

      EXTEND (sp, 4);
      /* XXX What does it mean for CRED to be 0?  */
      PUSHs (cred ? (SV *) cred->hook : &PL_sv_undef);
      /* XXX  Copy. (sigh) */
      PUSHs (sv_2mortal (newSVpv ((char *) data,
				  (STRLEN) data_len)));
      /* XXX IV may not be big enough for file offsets.  */
      PUSHs (sv_2mortal (newSViv ((IV) offs)));
      PUSHs (sv_2mortal (newRV (amountsv)));

      PUTBACK;
    }

  error_t pull ()
    {
      *amount = (mach_msg_type_number_t) SvIV (amountsv);
      return 0;
    }

  amountsv = newSViv ((IV) *amount);
  ret = call_sub (&sub, &push, &pull);
  SvREFCNT_dec (amountsv);
  return ret;
}

/* XXX I know I'll want to pass along the mach stuff once I figure out
   what it means.  */
kern_return_t
trivfs_S_io_read (struct trivfs_protid *cred,
		  mach_port_t reply, mach_msg_type_name_t reply_type,
		  data_t *data, mach_msg_type_number_t *data_len,
		  off_t offs, mach_msg_type_number_t amount)
{
  error_t ret;
  static struct cached_gv sub = { 0, "Hurd::Trivfs::_::io_read" };
  SV *buf;

  void push ()
    {
      dSP;

      EXTEND (sp, 4);
      /* XXX What does it mean for CRED to be 0?  */
      PUSHs (cred ? (SV *) cred->hook : &PL_sv_undef);
      PUSHs (sv_2mortal (newRV (buf)));
      /* XXX IV may not be big enough for file offsets.  */
      PUSHs (sv_2mortal (newSViv ((IV) offs)));
      PUSHs (sv_2mortal (newSViv ((IV) amount)));

      PUTBACK;
    }

  error_t pull ()
    {
      /* Copy. (sigh) */
      char *the_goods;
      STRLEN len;

      the_goods = SvPV (buf, len);
      *data_len = (mach_msg_type_number_t) len;

      /* Possibly allocate a new buffer.  Taken from hurd/trans/hello.c.  */
      if (*data_len < len)
	*data = (data_t) mmap (0, amount, PROT_READ|PROT_WRITE,
			       MAP_ANON, 0, 0);

      memcpy ((void *) *data, the_goods, len);
      return 0;
    }

  buf = newSVpv ("", 0);
  ret = call_sub (&sub, &push, &pull);
  SvREFCNT_dec (buf);
  return ret;
}

/* Change current read/write offset */
error_t
trivfs_S_io_seek (struct trivfs_protid *cred,
		  mach_port_t reply, mach_msg_type_name_t reply_type,
		  off_t offs, int whence, off_t *new_offs)
{
  error_t ret;
  static struct cached_gv sub = { 0, "Hurd::Trivfs::_::io_seek" };
  SV *offssv;

  void push ()
    {
      dSP;

      EXTEND (sp, 3);
      /* XXX What does it mean for CRED to be 0?  */
      PUSHs (cred ? (SV *) cred->hook : &PL_sv_undef);
      /* XXX IV may not be big enough for file offsets.  */
      PUSHs (sv_2mortal (newRV (offssv)));
      PUSHs (sv_2mortal (newSViv ((IV) whence)));

      PUTBACK;
    }

  error_t pull ()
    {
      *new_offs = (off_t) SvIV (offssv);
      return 0;
    }

  offssv = newSViv ((IV) offs);
  ret = call_sub (&sub, &push, &pull);
  SvREFCNT_dec (offssv);
  return ret;
}

kern_return_t
trivfs_S_io_readable (struct trivfs_protid *cred,
		      mach_port_t reply,
		      mach_msg_type_name_t replytype,
		      mach_msg_type_number_t *amount)
{
  error_t ret;
  static struct cached_gv sub = { 0, "Hurd::Trivfs::_::io_readable" };
  SV *amountsv;

  void push ()
    {
      dSP;

      EXTEND (sp, 2);
      /* XXX What does it mean for CRED to be 0?  */
      PUSHs (cred ? (SV *) cred->hook : &PL_sv_undef);
      PUSHs (sv_2mortal (newRV (amountsv)));

      PUTBACK;
    }

  error_t pull ()
    {
      *amount = (mach_msg_type_number_t) SvIV (amountsv);
      return 0;
    }

  amountsv = newSViv ((IV) *amount);
  ret = call_sub (&sub, &push, &pull);
  SvREFCNT_dec (amountsv);
  return ret;
}

error_t
trivfs_S_io_set_all_openmodes (struct trivfs_protid *cred,
			       mach_port_t reply,
			       mach_msg_type_name_t replytype,
			       int mode)
{
  static struct cached_gv sub
    = { 0, "Hurd::Trivfs::_::io_set_all_openmodes" };

  void push ()
    {
      dSP;

      EXTEND (sp, 2);
      /* XXX What does it mean for CRED to be 0?  */
      PUSHs (cred ? (SV *) cred->hook : &PL_sv_undef);
      PUSHs (sv_2mortal (newSViv ((IV) mode)));

      PUTBACK;
    }

  return call_sub (&sub, &push, 0);
}

kern_return_t
trivfs_S_io_get_openmodes (struct trivfs_protid *cred,
			   mach_port_t reply, mach_msg_type_name_t replytype,
			   int *bits)
{
  error_t ret;
  static struct cached_gv sub = { 0, "Hurd::Trivfs::_::io_get_openmodes" };
  SV *modessv;

  void push ()
    {
      dSP;

      EXTEND (sp, 2);
      /* XXX What does it mean for CRED to be 0?  */
      PUSHs (cred ? (SV *) cred->hook : &PL_sv_undef);
      PUSHs (sv_2mortal (newRV (modessv)));

      PUTBACK;
    }

  error_t pull ()
    {
      *bits = (mach_msg_type_number_t) SvIV (modessv);
      return 0;
    }

  modessv = newSViv ((IV) *bits);
  ret = call_sub (&sub, &push, &pull);
  SvREFCNT_dec (modessv);
  return ret;
}

kern_return_t
trivfs_S_io_set_some_openmodes (struct trivfs_protid *cred,
				mach_port_t reply,
				mach_msg_type_name_t replytype,
				int bits)
{
  static struct cached_gv sub
    = { 0, "Hurd::Trivfs::_::io_set_some_openmodes" };

  void push ()
    {
      dSP;

      EXTEND (sp, 2);
      /* XXX What does it mean for CRED to be 0?  */
      PUSHs (cred ? (SV *) cred->hook : &PL_sv_undef);
      PUSHs (sv_2mortal (newSViv ((IV) bits)));

      PUTBACK;
    }

  return call_sub (&sub, &push, 0);
}

kern_return_t
trivfs_S_io_clear_some_openmodes (struct trivfs_protid *cred,
				  mach_port_t reply,
				  mach_msg_type_name_t replytype,
				  int bits)
{
  static struct cached_gv sub
    = { 0, "Hurd::Trivfs::_::io_clear_some_openmodes" };

  void push ()
    {
      dSP;

      EXTEND (sp, 2);
      /* XXX What does it mean for CRED to be 0?  */
      PUSHs (cred ? (SV *) cred->hook : &PL_sv_undef);
      PUSHs (sv_2mortal (newSViv ((IV) bits)));

      PUTBACK;
    }

  return call_sub (&sub, &push, 0);
}

kern_return_t
trivfs_S_io_select (struct trivfs_protid *cred,
		    mach_port_t reply,
		    mach_msg_type_name_t replytype,
		    int *seltype)
{
  return EOPNOTSUPP;
}

/* Truncate file.  */
kern_return_t
trivfs_S_file_set_size (struct trivfs_protid *cred,
			mach_port_t reply, mach_msg_type_name_t reply_type,
			off_t size)
{
  static struct cached_gv sub = { 0, "Hurd::Trivfs::_::file_set_size" };

  void push ()
    {
      dSP;

      EXTEND (sp, 2);
      /* XXX What does it mean for CRED to be 0?  */
      PUSHs (cred ? (SV *) cred->hook : &PL_sv_undef);
      /* XXX IV may not be big enough for file offsets.  */
      PUSHs (sv_2mortal (newSViv ((IV) size)));

      PUTBACK;
    }

  return call_sub (&sub, &push, 0);
}

/* This will be called from libtrivfs to help construct the answer
   to an fsys_get_options RPC.  */
error_t
trivfs_append_args (struct trivfs_control *fsys,
		    char **argz, size_t *argz_len)
{
  croak ("Can't handle fsys_get_options RPC");
}


MODULE = Hurd::Trivfs	PACKAGE = Hurd::Trivfs

void
wait()
	CODE:
	{
	  volatile int x = 0;
	  fprintf (stderr, "process %d waiting\n", (int) getpid ());
	  while (!x) ;
	}

SV *
_startup(bootstrap, flags=0, control_bucket=0, protid_bucket=0)
	int	bootstrap
	int	flags
	SV *	control_bucket
	SV *	protid_bucket
	CODE:
	{
	  struct trivfs_control *cntl;
	  error_t err;

	  err = trivfs_startup (bootstrap, flags, 0, 0, 0, 0, &cntl);
	  if (err)
	    {
	      errno = err;
	      XSRETURN_UNDEF;
	    }

	  cntl->hook = alloc_hook (cntl, gv_stashpv ("Hurd::Trivfs", 1));
	  /* Add a reference because the trivfs machinery holds its own
	     reference via cntl->hook.
	     XXX This might be more robust with a DESTROY method.  */
	  RETVAL = SvREFCNT_inc ((SV *) cntl->hook);
	}
	OUTPUT:
	RETVAL

void
_go(cntl, timeout)
	SV *	cntl
	int timeout
	CODE:
	{
	  if (SvROK (cntl)
	      && sv_derived_from (cntl, "Hurd::Trivfs"))
	  {
	    struct trivfs_control *c
	      = (struct trivfs_control *)
	      ((struct hook *) SvIV (SvRV (cntl)))->orig;

	    ports_manage_port_operations_one_thread
	      (c->pi.bucket, trivfs_demuxer, timeout);
	  }
	}


MODULE = Hurd::Trivfs	PACKAGE = Hurd::Trivfs::Param

int
STORE(param, value)
	SV *	param
	int	value

int
FETCH(param)
	SV *	param


MODULE = Hurd::Trivfs	PACKAGE = Hurd::Trivfs::Peropen

SV *
cntl(po)
	SV *	po
	CODE:
	{
	  RETVAL = &PL_sv_undef;
	  if (SvROK (po)
	      && sv_derived_from (po, "Hurd::Trivfs::Peropen"))
	  {
	    struct trivfs_peropen *p
	      = (struct trivfs_peropen *)
	      ((struct hook *) SvIV (SvRV (po)))->orig;

	    if (p)
	      RETVAL = newSVsv ((SV *) p->cntl->hook);
	  }
	}
	OUTPUT:
	RETVAL

int
openmodes(po,value=0)
	SV *	po
	int	value
	CODE:
	{
	  RETVAL = 0;
	  if (SvROK (po)
	      && sv_derived_from (po, "Hurd::Trivfs::Peropen"))
	  {
	    struct trivfs_peropen *p
	      = (struct trivfs_peropen *)
	      ((struct hook *) SvIV (SvRV (po)))->orig;

	    if (items == 2)
	      p->openmodes = value;
	    else
	      RETVAL = p->openmodes;
	  }
	}
	OUTPUT:
	RETVAL


MODULE = Hurd::Trivfs	PACKAGE = Hurd::Trivfs::Protid

SV *
po(prot)
	SV *	prot
	CODE:
	{
	  RETVAL = &PL_sv_undef;
	  if (SvROK (prot)
	      && sv_derived_from (prot, "Hurd::Trivfs::Protid"))
	  {
	    /* XXX I want a handy macro for this...  */
	    struct trivfs_protid *p
	      = (struct trivfs_protid *)
	      ((struct hook *) SvIV (SvRV (prot)))->orig;

	    if (p)
	      RETVAL = newSVsv ((SV *) p->po->hook);
	  }
	}
	OUTPUT:
	RETVAL


MODULE = Hurd::Trivfs	PACKAGE = Hurd::Trivfs::_Hooky

SV *
hook(self,value=0)
	SV *	self
	SV *	value
	CODE:
	{
	  RETVAL = &PL_sv_undef;
	  if (SvROK (self)
	      && sv_derived_from (self, "Hurd::Trivfs::_Hooky"))
	  {
	    struct hook *h = (struct hook *) SvIV (SvRV (self));

	    if (items == 2)
	      {
		SvREFCNT_dec (h->hook);
		SvREFCNT_inc (value);
		h->hook = value;
	      }
	    else
	      RETVAL = newSVsv (h->hook);
	  }
	}
	OUTPUT:
	RETVAL


MODULE = Hurd::Trivfs	PACKAGE = Mach::Port

int
_deallocate(task, port)
	int	task
	int	port
	CODE:
	{
	  if (task == 0)
	    task = (int) mach_task_self ();
	  RETVAL = mach_port_deallocate ((mach_port_t) task,
					 (mach_port_t) port);
	}
	OUTPUT:
	RETVAL


MODULE = Hurd::Trivfs	PACKAGE = Mach::Task

int
_get_bootstrap_port(task=mach_task_self ())
	int	task
	CODE:
	{
	  mach_port_t ret;
	  task_get_bootstrap_port (task, &ret);
	  RETVAL = ret;
	}
	OUTPUT:
	RETVAL
