/* Let MiG do the work... */
#define FS_NOTIFY_INTRAN notice_handle_t notice_port_to_handle (fs_notify_t)
#define FS_NOTIFY_INTRAN_PAYLOAD notice_handle_t notice_payload_to_handle
#define FS_NOTIFY_DESTRUCTOR ports_port_deref (notice_handle_t)
#define FS_NOTIFY_IMPORTS import "notice_mig.h";
