/* Included in MiG server module.  We need this to convert ports to handles
   automatically.  */

/* We need to do that, as MiG won't understand us otherwise.  */
typedef struct notice_handle *notice_handle_t;

/* Intran function.  */
notice_handle_t notice_port_to_handle (mach_port_t);

/* Intran_payload function.  */
notice_handle_t notice_payload_to_handle (unsigned long payload);

/* For ports_port_deref ().  */
#include <hurd/ports.h>
